# projet-conception

<br/>
<br/>

## ANALYSE DES BESOINS

<br/>

### QUOI ?
	
**obligatoire :**
  * gérer un budget mensuel : entrées et sorties
  * les entrées doivent pouvoir être catégorisées (loisirs, famille, loyer/prêt immobilier, etc)
  * il doit être possible de "geler" une entrée : une entrée gelée n'est plus modifiable
  * l'application doit permettre d'éditer des statistiques mensuelles sur le pourcentage de dépenses par type, l'évolution  au fil du temps, etc

**facultative :**
		
  * possibilité de gérer un budget commun, et des budgets séparés
  * possibilité pour les parents de voir le budget de leurs enfants, mais pas l'inverse
  * possibilité de faire un budget prévisionnel, c'est à dire dans le futur, pour planifier des grosses dépenses (vacances, réparation de voiture, achat immobilier, etc)
  * possibilité de voir son solde bancaire et son évolution au fil du temps
  * possibilité d'ajouter des justificatifs (tickets de caisse, ticket de péage, facture, etc)
  *  possibilité de désigner une entrée comme étant récurrente (elle est automatiquement ajoutée tous les mois, par exemple votre abonnement à Internet)

		
### QUI ?
  * client : mon foyer
  * utilisateur : mon foyer

### COMMENT ?
  * context final : application type desktop
  * techno : Java + Spring Boot + Thymeleaf (rapididété de mise en place vis-à-vis de mes connaissances)
  * délai : avant de faire ma demande d'augmentation.

### USAGE ?

Usage personnel, prévu pour le long terme.


## USE CASE
![useCase](exports/use_case.png)


## CONCEPTION DE LA BDD

La base de données comprends 4 entités  : 

**Operation**
  * une opération avec un montant. Le montant est stoqué en integer dans la bdd (en centime).
  * l'opération peut être soit une dépense soit une recette (typeOfTransaction) : string RECEIPE ou SPEND enregistrés dans la bdd (implique deux setter personnalisés pour la classe Operation)
  * l'opération peut-être immuable, si true, son montant ne peut pas être modifié
  * comprend une date, qui peut être située dans l'avenir pour prévoir des dépenses.
  * recurrence : string DAY, MONTH, YEAR qui determineront la récurrence de l'opération dans le temps.
   

**Categorie**
  * la catégorie de l'opération. L'utilisateur pourra en ajouter à sa guise (si elle est inexistante elle sera crée automatiquement)
   

**User (option)**

  * utilisateur connecté ave un rôle.
  * rôle définie les actions possible :
    * parent = admin
    * enfant ne peut visualtiser créer et modifier seulement sur ses propres opérations
     

**justificatif (option)**
  * enregiste le path du fichier justificatif sur le serveur. 1 justificatif par opération.

![Diagramme-de-classe-entité](exports/diagramme%20de%20classes%20Entités.png)

## DIAGRAMME DE CLASSE
Je pars du principe que les utilisateurs seront stockés en dur dans la database, la famille ne risque pas de trop changer...

**Repository**

* chaque classe entité aura un repository avec le nom : *entité* + Repository. 
* La classe UserRepository aura une methode getUserByName() pour récupérer le nom du user et implémenter le UserDetailService (pour gérer les accès aux diférentes roots)
* Les repository étendent JpaRepository pour utiliser JPA pour l'accès à la BDD (plus rapide à coder)
  
**Services**
* Les services ont ce nom : *utilité* + service
* FileService permettera de sauvegarder les fichier et les supprimer (saveFile() - deleteFile())
* OperationService permet d'utiliser différents repository et methodes pour effectuer les opérations du CRUD en :
  *  controllant si l'utilisateur est children (même si la route devrait le bloquer) private car utilisé que dans cette classe, 
  *  d'appeller les méthodes de FileService lors du save
  *  de save avec le user connécté
  *  de créer une catégorie si elle n'existe pas (private car utilisé que dans la classe)
  *  de recalculer et enregistrer le total du compte banquaire
* StatService permet de faire tout les calcules de stats en fonction des différents filtres et date. Tout les calcules sont fait en centime. le front s'occupera d'afficher les valeur en euro avec centime.


**Controller**

* OperationManagerController permet d'envoyer du contenu à différents templates Thymeleaf pour effetuer un CRUD sur les Opération suivant l'utilisateur et de récupérer les formulaires et fichiers.
* StatController permet d'envoyer les résultats des stats suivant la route et les filtres choisis par l'utilisateur
* OperationViewerService permet d'afficher un calendrier avec les différentes opérations filtrable par utilisateur, par date et par catégorie

**nom de methode**

Les methodes sont nommé de cette façon :

* *find* + ... : rechercher des elements dans la bdd
* *save* + ... : enregestrer des elements dans la bdd
* *update* + ... : modifier des elements dans la bdd
* *delete* + ... : supprimer des elements dans la bdd
  <br/>
  <br/>
* *get* + ... : récupérer un résultat depuis une methode ou depuis le front
* *is* + ... : vérification d'une donnée (return un boolean)
* *display* + ... : envoyer des données aux templates thymleaf


![diagramme-de-classe](exports/diagramme_de_classe.png)